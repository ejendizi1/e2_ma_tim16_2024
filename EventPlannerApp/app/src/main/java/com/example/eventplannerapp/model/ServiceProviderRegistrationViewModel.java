package com.example.eventplannerapp.model;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import java.io.Closeable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceProviderRegistrationViewModel extends ViewModel {
    private String email;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private String role;
    private String profilePictureUri;

    private String companyEmail;
    private String companyName;
    private String companyAddress;
    private String companyPhone;
    private String companyDescription;
    private String companyPictureUri;
    private HashMap<String, WorkDay> workDays;
    private List<Category> categories;
    private List<EventType> eventTypes;
    public ServiceProviderRegistrationViewModel() {
    }

    public ServiceProviderRegistrationViewModel(String email, String password, String confirmPassword, String firstName, String lastName, String address, String phone, String role, String profilePictureUri, String companyEmail, String companyName, String companyAddress, String companyPhone, String companyDescription, String companyPictureUri, HashMap<String, WorkDay> workDays,List<Category>categories,List<EventType> eventTypes) {
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.role = role;
        this.profilePictureUri = profilePictureUri;
        this.companyEmail = companyEmail;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.companyDescription = companyDescription;
        this.companyPictureUri = companyPictureUri;
        this.workDays = workDays;
        this.categories=categories;
        this.eventTypes=eventTypes;
    }

    public ServiceProviderRegistrationViewModel(String email, String password, String confirmPassword, String firstName, String lastName, String address, String phone, String role, String profilePictureUri, String companyEmail, String companyName, String companyAddress, String companyPhone, String companyDescription, String companyPictureUri) {
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.role = role;
        this.profilePictureUri = profilePictureUri;
        this.companyEmail = companyEmail;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.companyDescription = companyDescription;
        this.companyPictureUri = companyPictureUri;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getCompanyPictureUri() {
        return companyPictureUri;
    }

    public void setCompanyPictureUri(String companyPictureUri) {
        this.companyPictureUri = companyPictureUri;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public HashMap<String, WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(HashMap<String, WorkDay> workDays) {
        this.workDays = workDays;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProfilePictureUri() {
        return profilePictureUri;
    }

    public void setProfilePictureUri(String profilePictureUri) {
        this.profilePictureUri = profilePictureUri;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }
}

