package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.eventplannerapp.R;

import java.util.ArrayList;
import java.util.List;

public class CreateEventActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_event);
		Toolbar toolbar = findViewById(R.id.toolbar1);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);




		Spinner spinner = findViewById(R.id.spinnerTypesEvents);

		List<String> items = new ArrayList<>();
		items.add("");
		items.add("Wedding");
		items.add("Birthday party");
		items.add("Coorporative party");
		items.add("Giving birth party");
		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);

	}

	public void submit(View view) {
		Intent intent = new Intent(this, ServiceProviderActivityEvent.class);
		startActivity(intent);
	}
}