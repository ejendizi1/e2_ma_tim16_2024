package com.example.eventplannerapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AlertDialog;

import com.example.eventplannerapp.activities.CategoryEditActivity;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.activities.EventTypeEditActivity;
import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Subcategory;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventTypeAdapter extends RecyclerView.Adapter<EventTypeAdapter.ViewHolder> {

    private List<EventType> eventtypes;
    private Context context;
    private LayoutInflater inflater;
    private static final ExecutorService executorService = Executors.newFixedThreadPool(3);

    public EventTypeAdapter(List<EventType> eventTypes, Context context) {
        this.eventtypes = eventTypes;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.event_type_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventType eventType = eventtypes.get(position);

        holder.eventTypeNameTV.setText(eventType.name);
        holder.eventTypeDescriptionTV.setText(eventType.description);
        if (eventType.isActive) {
            holder.deactivateEventTypeButton.setText("DEACTIVATE");
        } else {
            holder.deactivateEventTypeButton.setText("ACTIVATE");
        }

        executorService.submit(() -> {
            List<Subcategory> subcategories = (eventType.suggestedSC != null) ? eventType.suggestedSC : new ArrayList<>();

            // Update the RecyclerView adapter on the main thread
            holder.subcategoryRV.post(() -> {
                if (holder.subcategoryInfoAdapter == null) {
                    holder.subcategoryInfoAdapter = new SubcategoryInfoAdapter(subcategories,eventType.id,context);
                    holder.subcategoryRV.setAdapter(holder.subcategoryInfoAdapter);
                } else {
                    holder.subcategoryInfoAdapter.updateSubcategories(subcategories);
                }
            });
        });

        holder.editEventTypeButton.setOnClickListener(v -> {
            Intent intent = new Intent(context, EventTypeEditActivity.class);
            intent.putExtra("EventTypeId", eventType.id);
            intent.putExtra("EventTypeDescription", eventType.description);
            context.startActivity(intent);
        });
        holder.deactivateEventTypeButton.setOnClickListener(v -> {

            new AlertDialog.Builder(context)
                    .setTitle("Confirm status change")
                    .setMessage("Are you sure you want to " + (eventType.isActive ? "deactivate" : "activate") + " this event type?")
                    .setPositiveButton("Yes", (dialog, which) -> {

                        boolean newStatus = !eventType.isActive;
                        eventType.isActive = newStatus;


                        DatabaseReference eventTypeRef = FirebaseDatabase.getInstance().getReference("EventTypes").child(eventType.id);
                        eventTypeRef.setValue(eventType)
                                .addOnSuccessListener(aVoid -> {
                                    holder.deactivateEventTypeButton.setText(newStatus ? "DEACTIVATE" : "ACTIVATE");
                                    String action = newStatus ? "activated" : "deactivated";
                                    Toast.makeText(context, "Event type " + eventType.name + " " + action, Toast.LENGTH_SHORT).show();
                                })
                                .addOnFailureListener(e -> Toast.makeText(context, "Error updating status", Toast.LENGTH_SHORT).show());
                    })
                    .setNegativeButton("No", (dialog, which) -> {

                        dialog.dismiss();
                    })
                    .show();
        });


    }

    @Override
    public int getItemCount() {
        return eventtypes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView eventTypeNameTV;
        TextView eventTypeDescriptionTV;
        Button editEventTypeButton;
        Button deactivateEventTypeButton;

        RecyclerView subcategoryRV;
        SubcategoryInfoAdapter subcategoryInfoAdapter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            eventTypeNameTV = itemView.findViewById(R.id.etNameTextView);
            eventTypeDescriptionTV = itemView.findViewById(R.id.etDescriptionTextView);
            editEventTypeButton = itemView.findViewById(R.id.editEventTypeButton);
            deactivateEventTypeButton = itemView.findViewById(R.id.deactivateEventTypeButton);
            subcategoryRV = itemView.findViewById(R.id.subcategoryRecyclerView);
            subcategoryRV.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        }
    }

    public void updateEventTypes(List<EventType> newEventTypes) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return eventtypes.size();
            }

            @Override
            public int getNewListSize() {
                return newEventTypes.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return eventtypes.get(oldItemPosition).id.equals(newEventTypes.get(newItemPosition).id);
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return eventtypes.get(oldItemPosition).equals(newEventTypes.get(newItemPosition));
            }
        });

        eventtypes.clear();
        eventtypes.addAll(newEventTypes);
        diffResult.dispatchUpdatesTo(this);
    }
}

