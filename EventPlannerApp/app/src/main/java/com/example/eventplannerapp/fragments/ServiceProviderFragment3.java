package com.example.eventplannerapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.ServiceProviderActivity;
import com.example.eventplannerapp.model.ServiceProviderRegistrationViewModel;
import com.example.eventplannerapp.model.WorkDay;

import java.util.HashMap;
import java.util.Locale;


public class ServiceProviderFragment3 extends Fragment {
    private HashMap<String, WorkDay> workDayMap = new HashMap<>();

    private ServiceProviderRegistrationViewModel viewModel;
    public ServiceProviderFragment3() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ((ServiceProviderActivity) requireActivity()).getViewModel();


    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_provider3, container, false);
        Button buttonNext = rootView.findViewById(R.id.buttonNext);
        CheckBox mondayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDayMonday);
        TimePicker mondayFromTimePicker = rootView.findViewById(R.id.mondayFromTime);
        TimePicker mondayToTimePicker = rootView.findViewById(R.id.mondayToTime);
        CheckBox tuesdayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDayTuesday);
        TimePicker tuesdayFromTimePicker = rootView.findViewById(R.id.tuesdayFromTime);
        TimePicker tuesdayToTimePicker = rootView.findViewById(R.id.tuesdayToTime);
        CheckBox wednesdayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDayWednesday);
        TimePicker wednesdayFromTimePicker = rootView.findViewById(R.id.wednesdayFromTime);
        TimePicker wednesdayToTimePicker = rootView.findViewById(R.id.wednesdayToTime);
        CheckBox thursdayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDayThursday);
        TimePicker thursdayFromTimePicker = rootView.findViewById(R.id.thursdayFromTime);
        TimePicker thursdayToTimePicker = rootView.findViewById(R.id.thursdayToTime);
        CheckBox fridayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDayFriday);
        TimePicker fridayFromTimePicker = rootView.findViewById(R.id.fridayFromTime);
        TimePicker fridayToTimePicker = rootView.findViewById(R.id.fridayToTime);
        CheckBox saturdayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDaySaturday);
        TimePicker saturdayFromTimePicker = rootView.findViewById(R.id.saturdayFromTime);
        TimePicker saturdayToTimePicker = rootView.findViewById(R.id.saturdayToTime);
        CheckBox sundayCheckBox = rootView.findViewById(R.id.checkBoxWorkingDaySunday);
        TimePicker sundayFromTimePicker = rootView.findViewById(R.id.sundayFromTime);
        TimePicker sundayToTimePicker = rootView.findViewById(R.id.sundayToTime);




        mondayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Monday")) {
                WorkDay mondayWorkDay = workDayMap.get("Monday");
                mondayWorkDay.setFromTime(getTimeFromTimePicker(mondayFromTimePicker));
                workDayMap.put("Monday", mondayWorkDay);
            }
        });

        mondayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Monday")) {
                WorkDay mondayWorkDay = workDayMap.get("Monday");
                mondayWorkDay.setToTime(getTimeFromTimePicker(mondayToTimePicker));
                workDayMap.put("Monday", mondayWorkDay);
            }
        });

        tuesdayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Tuesday")) {
                WorkDay tuesdayWorkDay = workDayMap.get("Tuesday");
                tuesdayWorkDay.setFromTime(getTimeFromTimePicker(tuesdayFromTimePicker));
                workDayMap.put("Tuesday", tuesdayWorkDay);
            }
        });

        tuesdayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Tuesday")) {
                WorkDay tuesdayWorkDay = workDayMap.get("Tuesday");
                tuesdayWorkDay.setToTime(getTimeFromTimePicker(tuesdayToTimePicker));
                workDayMap.put("Tuesday", tuesdayWorkDay);
            }
        });

// Ponovite isti postupak za srijedu
        wednesdayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Wednesday")) {
                WorkDay wednesdayWorkDay = workDayMap.get("Wednesday");
                wednesdayWorkDay.setFromTime(getTimeFromTimePicker(wednesdayFromTimePicker));
                workDayMap.put("Wednesday", wednesdayWorkDay);
            }
        });

        wednesdayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Wednesday")) {
                WorkDay wednesdayWorkDay = workDayMap.get("Wednesday");
                wednesdayWorkDay.setToTime(getTimeFromTimePicker(wednesdayToTimePicker));
                workDayMap.put("Wednesday", wednesdayWorkDay);
            }
        });


        // Za četvrtak
        thursdayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Thursday")) {
                WorkDay thursdayWorkDay = workDayMap.get("Thursday");
                thursdayWorkDay.setFromTime(getTimeFromTimePicker(thursdayFromTimePicker));
                workDayMap.put("Thursday", thursdayWorkDay);
            }
        });

        thursdayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Thursday")) {
                WorkDay thursdayWorkDay = workDayMap.get("Thursday");
                thursdayWorkDay.setToTime(getTimeFromTimePicker(thursdayToTimePicker));
                workDayMap.put("Thursday", thursdayWorkDay);
            }
        });

// Za petak
        fridayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Friday")) {
                WorkDay fridayWorkDay = workDayMap.get("Friday");
                fridayWorkDay.setFromTime(getTimeFromTimePicker(fridayFromTimePicker));
                workDayMap.put("Friday", fridayWorkDay);
            }
        });

        fridayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Friday")) {
                WorkDay fridayWorkDay = workDayMap.get("Friday");
                fridayWorkDay.setToTime(getTimeFromTimePicker(fridayToTimePicker));
                workDayMap.put("Friday", fridayWorkDay);
            }
        });

// Za subotu
        saturdayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Saturday")) {
                WorkDay saturdayWorkDay = workDayMap.get("Saturday");
                saturdayWorkDay.setFromTime(getTimeFromTimePicker(saturdayFromTimePicker));
                workDayMap.put("Saturday", saturdayWorkDay);
            }
        });

        saturdayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Saturday")) {
                WorkDay saturdayWorkDay = workDayMap.get("Saturday");
                saturdayWorkDay.setToTime(getTimeFromTimePicker(saturdayToTimePicker));
                workDayMap.put("Saturday", saturdayWorkDay);
            }
        });

// Za nedjelju
        sundayFromTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Sunday")) {
                WorkDay sundayWorkDay = workDayMap.get("Sunday");
                sundayWorkDay.setFromTime(getTimeFromTimePicker(sundayFromTimePicker));
                workDayMap.put("Sunday", sundayWorkDay);
            }
        });

        sundayToTimePicker.setOnTimeChangedListener((view, hourOfDay, minute) -> {
            if (workDayMap.containsKey("Sunday")) {
                WorkDay sundayWorkDay = workDayMap.get("Sunday");
                sundayWorkDay.setToTime(getTimeFromTimePicker(sundayToTimePicker));
                workDayMap.put("Sunday", sundayWorkDay);
            }
        });





        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("PRE BIND", String.valueOf(workDayMap));
                viewModel.setWorkDays(workDayMap);
                Log.d("Posle BIND", String.valueOf(viewModel.getWorkDays()));
                FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, ServiceProviderFragment4.newInstance())
                        .addToBackStack(null)
                        .commit();
            }
        });
        return rootView;
    }


    private String getTimeFromTimePicker(TimePicker timePicker) {
        int hour = timePicker.getHour();
        int minute = timePicker.getMinute();

        // format"HH:MM"
        return String.format(Locale.getDefault(), "%02d:%02d", hour, minute);
    }

}
