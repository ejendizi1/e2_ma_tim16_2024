package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Subcategory;
import com.example.eventplannerapp.model.SubcategoryType;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CreateNewSubcategoryActivity extends AppCompatActivity {
    private EditText nameEditText, descriptionEditText;
    private String categoryId;
    private Spinner subcategoryTypeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_create_new_subcategory);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Intent intent = getIntent();
        categoryId = intent.getStringExtra("CategoryId");
        subcategoryTypeSpinner = findViewById(R.id.subcategoryTypeSpinner);
        nameEditText = findViewById(R.id.editTextName);
        descriptionEditText = findViewById(R.id.editTextDescription);
        Button submitButton = findViewById(R.id.buttonSubmit);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.subcategory_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subcategoryTypeSpinner.setAdapter(adapter);

        submitButton.setOnClickListener(v -> {
            String subcategoryTypeString = subcategoryTypeSpinner.getSelectedItem().toString();


            SubcategoryType subcategoryType;
            switch (subcategoryTypeString) {
                case "PRODUCT":
                    subcategoryType = SubcategoryType.PRODUCT;
                    break;
                case "SERVICE":
                default:
                    subcategoryType = SubcategoryType.SERVICE;
                    break;
            }

            String subcategoryName = nameEditText.getText().toString().trim();
            String subcategoryDescription = descriptionEditText.getText().toString().trim();

            if (!subcategoryName.isEmpty() && !subcategoryDescription.isEmpty()) {
                String id = FirebaseDatabase.getInstance().getReference("Subcategories").push().getKey();
                Subcategory newSubcategory = new Subcategory(id, subcategoryName, subcategoryDescription, subcategoryType);

                // pristupam postojećoj listi potkategorija i ažuriram je novom potkategorijom
                DatabaseReference categoryRef = FirebaseDatabase.getInstance().getReference("Categories").child(categoryId).child("Subcategories");
                categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        List<Subcategory> subcategoryList = new ArrayList<>();
                        if (snapshot.exists()) {
                            for (DataSnapshot subcategorySnapshot : snapshot.getChildren()) {
                                Subcategory existingSubcategory = subcategorySnapshot.getValue(Subcategory.class);
                                if (existingSubcategory != null) {
                                    subcategoryList.add(existingSubcategory);
                                }
                            }
                        }


                        subcategoryList.add(newSubcategory);

                        categoryRef.setValue(subcategoryList).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(CreateNewSubcategoryActivity.this, "Subcategory added successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(CreateNewSubcategoryActivity.this, "Failed to add subcategory", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(CreateNewSubcategoryActivity.this, "Error reading subcategories", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(CreateNewSubcategoryActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
            }
        });
    }
}