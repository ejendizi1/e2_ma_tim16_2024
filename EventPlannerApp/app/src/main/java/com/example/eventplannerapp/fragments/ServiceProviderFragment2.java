package com.example.eventplannerapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.ServiceProviderActivity;
import com.example.eventplannerapp.model.ServiceProviderRegistrationViewModel;


public class ServiceProviderFragment2 extends Fragment {
    private EditText editTextEmail;
    private EditText editTextName;
    private EditText editTextDescription;
    private EditText editTextAddress;
    private EditText editTextPhone;
    private ActivityResultLauncher<String> mGetContent;
    private ImageView mImageViewSelectedPicture;

    private ServiceProviderRegistrationViewModel viewModel;
    public ServiceProviderFragment2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ((ServiceProviderActivity) requireActivity()).getViewModel();


    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_provider2, container, false);


        Button buttonNext = rootView.findViewById(R.id.buttonNext);


        editTextEmail = rootView.findViewById(R.id.editTextEdit);
        editTextName = rootView.findViewById(R.id.editTextName);
        editTextDescription = rootView.findViewById(R.id.editTextDescription);
        editTextAddress = rootView.findViewById(R.id.editTextAddress);
        editTextPhone = rootView.findViewById(R.id.editTextPhoneNumber);


        // Postavljanje unesenih podataka iz ViewModel-a u polja za unos
        editTextEmail.setText(viewModel.getCompanyEmail() != null ? viewModel.getCompanyEmail() : "");
        editTextDescription.setText(viewModel.getCompanyDescription() != null ? viewModel.getCompanyDescription() : "");
        editTextName.setText(viewModel.getCompanyName() != null ? viewModel.getCompanyName() : "");
        editTextAddress.setText(viewModel.getCompanyAddress() != null ? viewModel.getCompanyAddress() : "");
        editTextPhone.setText(viewModel.getCompanyPhone() != null ? viewModel.getCompanyPhone() : "");


        Button uploadButton = rootView.findViewById(R.id.buttonUploadProfilePicture);
        mImageViewSelectedPicture = rootView.findViewById(R.id.imageViewSelectedPicture);

        uploadButton.setOnClickListener(v -> openGallery());


        mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    mImageViewSelectedPicture.setVisibility(View.VISIBLE);
                    mImageViewSelectedPicture.setImageURI(uri);
                    viewModel.setProfilePictureUri(uri.toString());
                });



        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setCompanyEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setCompanyDescription(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });



        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setCompanyName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });



        editTextAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setCompanyAddress(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setCompanyPhone(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fieldsValid = validateFields();
                if (viewModel.getCompanyPictureUri() == null) {
                    viewModel.setCompanyPictureUri("");
                }

                if (fieldsValid) {
                    // Kreiranje nove niti za prelazak na sledeći fragment
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // Kod za prelazak na sledeći fragment
                            requireActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                                    fragmentManager.beginTransaction()
                                            .replace(R.id.fragment_container, new ServiceProviderFragment3())
                                            .addToBackStack(null)
                                            .commit();
                                }
                            });
                        }
                    }).start();
                } else {
                    if (!fieldsValid) {
                        Toast.makeText(requireContext(), "All fields are required.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



        return rootView;
    }


    private boolean validateFields() {
        String email = editTextEmail.getText().toString();
        String description = editTextDescription.getText().toString();
        String name = editTextName.getText().toString();
        String address = editTextAddress.getText().toString();
        String phone = editTextPhone.getText().toString();

        if (email.isEmpty() || description.isEmpty() || name.isEmpty()
                 || address.isEmpty() || phone.isEmpty()) {
            return false;
        }

        return true;
    }

    private void openGallery() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                mGetContent.launch("image/*");
            }
        });
        thread.start();
    }

}
