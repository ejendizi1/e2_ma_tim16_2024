package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplannerapp.adapters.CategoryAdapter;
import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Category;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CategoryManagementViewActivity extends BaseActivity {
    private CategoryAdapter adapter;
    private DatabaseReference databaseCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContent(R.layout.activity_category_management_view);
        //setContentView(R.layout.activity_category_management_view);

        databaseCategories = FirebaseDatabase.getInstance().getReference("Categories");

        RecyclerView recyclerView = findViewById(R.id.categoryRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CategoryAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(adapter);

        loadCategoriesFromFirebase();
    }

    public void AddCategory(View view) {
        Intent intent = new Intent(this, CreateNewCategoryActivity.class);
        startActivity(intent);
    }

    private void loadCategoriesFromFirebase() {

        databaseCategories.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                new ParseCategoriesTask().execute(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(CategoryManagementViewActivity.this, "Failed to load categories", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private class ParseCategoriesTask extends AsyncTask<DataSnapshot, Void, List<Category>> {
        @Override
        protected List<Category> doInBackground(DataSnapshot... snapshots) {
            List<Category> categories = new ArrayList<>();
            for (DataSnapshot snapshot : snapshots[0].getChildren()) {
                Category category = snapshot.getValue(Category.class);
                categories.add(category);
            }
            return categories;
        }

        @Override
        protected void onPostExecute(List<Category> categories) {
            adapter.updateCategories(categories);
        }
    }
}
