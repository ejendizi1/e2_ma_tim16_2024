package com.example.eventplannerapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplannerapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEventBudget#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEventBudget extends Fragment {

	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;

	public FragmentEventBudget() {
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment FragmentEventBudget.
	 */
	// TODO: Rename and change types and number of parameters
	public static FragmentEventBudget newInstance(String param1, String param2) {
		FragmentEventBudget fragment = new FragmentEventBudget();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_event_budget, container, false);
		Button buttonFinalCosts = view.findViewById(R.id.buttonFinalCosts);


		buttonFinalCosts.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentEventCosts fragmentEventCosts = new FragmentEventCosts();
				FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				((FragmentTransaction) transaction).replace(R.id.fragment_container, fragmentEventCosts);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});


		return view;
	}
	public void onAddButtonClick(View view) {
		Toast.makeText(getContext(), "Usluga je dodata u listu troskova", Toast.LENGTH_SHORT).show();
	}



}