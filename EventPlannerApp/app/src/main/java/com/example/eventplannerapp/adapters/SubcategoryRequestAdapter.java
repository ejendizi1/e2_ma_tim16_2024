package com.example.eventplannerapp.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.CreateNewSubcategoryActivity;
import com.example.eventplannerapp.model.Company;
import com.example.eventplannerapp.model.Subcategory;
import com.example.eventplannerapp.model.SubcategoryRequest;
import com.example.eventplannerapp.model.User;
import com.example.eventplannerapp.model.UserRequest;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SubcategoryRequestAdapter extends RecyclerView.Adapter<SubcategoryRequestAdapter.SubcategoryRequestViewHolder> {

    private ArrayList<SubcategoryRequest> subList;
    private Context context;
    private FirebaseAuth mAuth;
    private Executor executor,executor1;



    public SubcategoryRequestAdapter(ArrayList<SubcategoryRequest> subList, Context context) {
        this.subList = subList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
        executor = Executors.newSingleThreadExecutor();
        executor1 = Executors.newSingleThreadExecutor();
    }

    @NonNull
    @Override
    public SubcategoryRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_request_card, parent, false);
        return new SubcategoryRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubcategoryRequestViewHolder holder, int position) {
        SubcategoryRequest sub = subList.get(position);
        holder.nameTextView.setText(sub.getName());
        holder.descriptionTextView.setText(sub.getDescription());
        holder.categoryNameTextView.setText(sub.getCategoryName());
        holder.typeTextView.setText(sub.getType().toString());

        holder.acceptButton.setOnClickListener(v -> executor.execute(() -> acceptSubcategoryRequest(sub)));
        holder.editButton.setOnClickListener(v -> showEditDialog(sub));
    }

    @Override
    public int getItemCount() {
        return subList.size();
    }

    public class SubcategoryRequestViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionTextView, nameTextView, typeTextView,categoryNameTextView;

        MaterialButton acceptButton,editButton;

        public SubcategoryRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            descriptionTextView = itemView.findViewById(R.id.subcategoryDescriptionTextView);
            nameTextView = itemView.findViewById(R.id.subcategoryNameTextView);
            categoryNameTextView = itemView.findViewById(R.id.categoryNameTextView);
            typeTextView = itemView.findViewById(R.id.subcategoryTypeTextView);
            acceptButton = itemView.findViewById(R.id.acceptButton);
            editButton = itemView.findViewById(R.id.editButton);

        }
    }
    private void acceptSubcategoryRequest(SubcategoryRequest sub) {

            String id = FirebaseDatabase.getInstance().getReference("Subcategories").push().getKey();
            Subcategory newSubcategory = new Subcategory(id, sub.getName(), sub.getDescription(), sub.getType());

            // pristupam postojećoj listi potkategorija i ažuriram je novom potkategorijom
            DatabaseReference categoryRef = FirebaseDatabase.getInstance().getReference("Categories").child(sub.getCategoryId()).child("Subcategories");
            categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    List<Subcategory> subcategoryList = new ArrayList<>();
                    if (snapshot.exists()) {
                        for (DataSnapshot subcategorySnapshot : snapshot.getChildren()) {
                            Subcategory existingSubcategory = subcategorySnapshot.getValue(Subcategory.class);
                            if (existingSubcategory != null) {
                                subcategoryList.add(existingSubcategory);
                            }
                        }
                    }

                    subcategoryList.add(newSubcategory);

                    categoryRef.setValue(subcategoryList).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText((Activity)context, "Subcategory added successfully", Toast.LENGTH_SHORT).show();

                            updateRequestStatus(sub);
                        } else {
                            Toast.makeText((Activity)context, "Failed to add subcategory", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText((Activity)context, "Error reading subcategories", Toast.LENGTH_SHORT).show();
                }
            });

    }
    private void updateRequestStatus(SubcategoryRequest sub) {
        executor.execute(() -> {
            sub.setAdminVerified(true);
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("SubcategoriesRequest").child(sub.id);
            userRef.child("adminVerified").setValue(true)
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.e("UserRequestAdapter", "Failed to update subcategory status: " + task.getException().getMessage());
                        }
                    });
        });
    }

    private void showEditDialog(SubcategoryRequest sub) {
        Activity activity = (Activity) context;
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_subcategory_request, null);

        EditText editName = dialogView.findViewById(R.id.editSubcategoryName);
        EditText editDescription = dialogView.findViewById(R.id.editSubcategoryDescription);

        // Popunite EditText polja trenutnim vrednostima
        editName.setText(sub.getName());
        editDescription.setText(sub.getDescription());

        new AlertDialog.Builder(context)
                .setView(dialogView)
                .setPositiveButton("ACCEPT", (dialog, which) -> {
                    String newName = editName.getText().toString().trim();
                    String newDescription = editDescription.getText().toString().trim();

                    if (!newName.isEmpty() && !newDescription.isEmpty()) {
                        sub.setName(newName);
                        sub.setDescription(newDescription);
                        executor.execute(() -> acceptSubcategoryRequest(sub));
                    } else {
                        Toast.makeText(context, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

}

