package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.model.Subcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CreateNewEventTypeActivity extends AppCompatActivity {

    private EditText eventTypeNameEditText;
    private EditText eventTypeDescriptionEditText;
    private DatabaseReference databaseEventTypes;
    private DatabaseReference subcategoriesRef;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Button addEventTypeButton;
    private Button addSubcategoryButton;
    private Spinner subcategorySpinner;
    private ListView subcategoryListView;
    private List<String> availableSubcategories;

    private List<Subcategory> allSubcategories;
    private List<String> recommendedSubcategories;
    private ArrayAdapter<String> subcategoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_event_type);

        allSubcategories = new ArrayList<>();
        recommendedSubcategories = new ArrayList<>();
        databaseEventTypes = FirebaseDatabase.getInstance().getReference("EventTypes");
        subcategoriesRef = FirebaseDatabase.getInstance().getReference("Categories");

        eventTypeNameEditText = findViewById(R.id.editTextNameT);
        eventTypeDescriptionEditText = findViewById(R.id.editTextDescriptionT);
        addEventTypeButton = findViewById(R.id.submitButton);
        addSubcategoryButton = findViewById(R.id.addSubcategoryButton);
        subcategorySpinner = findViewById(R.id.subcategorySpinner);
        subcategoryListView = findViewById(R.id.subcategoryListView);

        subcategoryListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recommendedSubcategories);
        subcategoryListView.setAdapter(subcategoryListAdapter);

        loadAllSubcategories();

        addEventTypeButton.setOnClickListener(v -> executorService.submit(() -> saveEventType()));

        addSubcategoryButton.setOnClickListener(v -> addSelectedSubcategoryToList());
    }


    private void loadAllSubcategories() {
        subcategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                availableSubcategories = new ArrayList<>();

                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    DataSnapshot subcategoriesSnapshot = categorySnapshot.child("Subcategories");

                    for (DataSnapshot subcategorySnapshot : subcategoriesSnapshot.getChildren()) {
                        Subcategory subcategory = subcategorySnapshot.getValue(Subcategory.class);
                        if (subcategory != null) {
                            allSubcategories.add(subcategory);
                            availableSubcategories.add(subcategory.Name);
                        }
                    }
                }

                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(CreateNewEventTypeActivity.this,
                        android.R.layout.simple_spinner_item, availableSubcategories);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subcategorySpinner.setAdapter(spinnerAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(CreateNewEventTypeActivity.this, "Failed to load subcategories", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void addSelectedSubcategoryToList() {
        String selectedSubcategoryName = subcategorySpinner.getSelectedItem().toString();

        if (!recommendedSubcategories.contains(selectedSubcategoryName)) {
            recommendedSubcategories.add(selectedSubcategoryName);
            subcategoryListAdapter.notifyDataSetChanged();


            availableSubcategories.remove(selectedSubcategoryName);
            ((ArrayAdapter)subcategorySpinner.getAdapter()).notifyDataSetChanged();
        } else {
            Toast.makeText(CreateNewEventTypeActivity.this, "This subcategory is already added", Toast.LENGTH_LONG).show();
        }
    }


    private void saveEventType() {
        String name = eventTypeNameEditText.getText().toString().trim();
        String description = eventTypeDescriptionEditText.getText().toString().trim();

        if (name.isEmpty() || description.isEmpty()) {
            runOnUiThread(() -> Toast.makeText(CreateNewEventTypeActivity.this, "You must enter name and description", Toast.LENGTH_LONG).show());
            return;
        }

        String id = databaseEventTypes.push().getKey();
        EventType eventType = new EventType(id, name, description, true, new ArrayList<>());

        for (String subcategoryName : recommendedSubcategories) {
            for (Subcategory subcategory : allSubcategories) {
                if (subcategory.Name.equals(subcategoryName)) {
                    eventType.suggestedSC.add(subcategory);
                    break;
                }
            }
        }

        databaseEventTypes.child(id).setValue(eventType).addOnCompleteListener(task -> {
            runOnUiThread(() -> {
                if (task.isSuccessful()) {
                    Toast.makeText(CreateNewEventTypeActivity.this, "Event type added", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(CreateNewEventTypeActivity.this, "Event type was not added", Toast.LENGTH_LONG).show();
                }
            });
        });
    }
}
