package com.example.eventplannerapp.activities;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CategoryEditActivity extends AppCompatActivity {
    EditText editTextName, editTextDescription;
    String categoryId;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_category_edit);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        editTextName = findViewById(R.id.editTextName);
        editTextDescription = findViewById(R.id.editTextDescription);
        Button updateButton = findViewById(R.id.buttonUpdate);

        Intent intent = getIntent();
        categoryId = intent.getStringExtra("CategoryId");
        String categoryName = intent.getStringExtra("CategoryName");
        String categoryDescription = intent.getStringExtra("CategoryDescription");

        editTextName.setText(categoryName);
        editTextDescription.setText(categoryDescription);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executorService.submit(() -> {
                    updateCategoryInFirebase();
                });
            }
        });
    }

    private void updateCategoryInFirebase() {
        String newName = editTextName.getText().toString();
        String newDescription = editTextDescription.getText().toString();

        // Update in Firebase
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Categories").child(categoryId);
        Map<String, Object> updates = new HashMap<>();
        updates.put("Name", newName);
        updates.put("Description", newDescription);


        databaseReference.updateChildren(updates).addOnCompleteListener(task -> {

            runOnUiThread(() -> {
                if (task.isSuccessful()) {
                    Toast.makeText(CategoryEditActivity.this, "Category updated successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(CategoryEditActivity.this, "Failed to update category", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }
}