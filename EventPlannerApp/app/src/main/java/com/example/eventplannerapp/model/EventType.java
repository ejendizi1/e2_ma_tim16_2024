package com.example.eventplannerapp.model;

import com.example.eventplannerapp.model.Subcategory;

import java.io.Serializable;
import java.util.List;

public class EventType implements Serializable {
    public String id;
    public String name;
    public String description;
    public List<Subcategory> suggestedSC;
    public Boolean isActive = true;

    public EventType() {
    }

    public EventType(String id, String name, String description,boolean isActive, List<Subcategory> suggestedSC) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isActive=isActive;
        this.suggestedSC = suggestedSC;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

