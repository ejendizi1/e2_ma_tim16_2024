package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.eventplannerapp.R;
import com.google.android.material.navigation.NavigationView;

public class BaseActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private SharedPreferences preferences;
    private static final String PREF_NAME = "UserPreferences";
    private static final String ROLE_KEY = "user_role";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        setupNavigationDrawer();
        preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        String role = preferences.getString(ROLE_KEY, "GUEST");

        Log.d("BaseActivity", "Loaded role: " + role);

        adjustDrawerForRole(role);
    }

    protected void setActivityContent(int layoutResID) {
        FrameLayout contentFrame = findViewById(R.id.content_frame);
        getLayoutInflater().inflate(layoutResID, contentFrame, true);
    }

    public void setupNavigationDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_item1) {
                    // LoginActivity
                    Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;
                    //Categories management
                } else if (id == R.id.nav_item2) {
                    Intent intent = new Intent(BaseActivity.this, CategoryManagementViewActivity.class);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;
                }
                else if (id == R.id.nav_item3) {
                    Intent intent = new Intent(BaseActivity.this, SubcategoriesRequestManagementActivity.class);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;
                }
                else if (id == R.id.nav_item4) {
                    Intent intent = new Intent(BaseActivity.this, EventTypeManagementActivity.class);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;
                }
                else if (id == R.id.nav_item5) {
                    Intent intent = new Intent(BaseActivity.this, UserManagementActivity.class);
                    startActivity(intent);
                    drawerLayout.closeDrawers();
                    return true;
                }

                return false;
            }
        });
    }

    public void refreshNavigationDrawer() {

        String role = preferences.getString(ROLE_KEY, "GUEST");

        adjustDrawerForRole(role);
    }

    public void adjustDrawerForRole(String role) {
        if (navigationView != null) {
            Menu navMenu = navigationView.getMenu();
            MenuItem loginItem = navMenu.findItem(R.id.nav_item1);
            MenuItem categoryItem = navMenu.findItem(R.id.nav_item2);
            MenuItem subcItem = navMenu.findItem(R.id.nav_item3);
            MenuItem eventTypeItem = navMenu.findItem(R.id.nav_item4);
            MenuItem userItem = navMenu.findItem(R.id.nav_item5);

            switch (role) {
                case "ADMIN":
                    loginItem.setVisible(true);
                    categoryItem.setVisible(true);
                    subcItem.setVisible(true);
                    eventTypeItem.setVisible(true);
                    userItem.setVisible(true);
                    break;
                    //ovde za organizatora dogadjaja(event organizator)
                case "EO":
                    loginItem.setVisible(false);
                    categoryItem.setVisible(false);
                    subcItem.setVisible(false);
                    eventTypeItem.setVisible(false);
                    userItem.setVisible(false);
                    break;
                    //ovde za vlasnika pup(service provider organizator)
                case "SPO":
                    //dodajte i vase iteme pa stavite visible na true za vase

                    loginItem.setVisible(false);
                    categoryItem.setVisible(false);
                    subcItem.setVisible(false);
                    eventTypeItem.setVisible(false);
                    userItem.setVisible(false);
                    break;
                case "GUEST":
                    loginItem.setVisible(false);
                    categoryItem.setVisible(false);
                    subcItem.setVisible(false);
                    eventTypeItem.setVisible(true);
                    userItem.setVisible(false);
                    break;
                    //dodajte za jos uloga koja vam treba
                //default mora da stoji samo ovako zbog switch iako je neprijavljeni guest uvek!!
                default:
                    loginItem.setVisible(false);
                    categoryItem.setVisible(false);
                    subcItem.setVisible(false);
                    eventTypeItem.setVisible(false);
                    userItem.setVisible(false);
                    break;
            }
        }
    }
}
