package com.example.eventplannerapp.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Subcategory;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SubcategoryInfoAdapter extends RecyclerView.Adapter<SubcategoryInfoAdapter.ViewHolder> {
    private List<Subcategory> subcategories;
    private Context context;
    private String eventTypeId;
    private LayoutInflater inflater;
    private final Handler handler = new Handler(Looper.getMainLooper());
    public SubcategoryInfoAdapter(List<Subcategory> subcategories) {
        this.subcategories = subcategories != null ? subcategories : new ArrayList<>();
    }

    public SubcategoryInfoAdapter(List<Subcategory> dataSet,String eventTypeId, Context context) {
        this.subcategories = dataSet;
        this.eventTypeId = eventTypeId;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public SubcategoryInfoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.subcategory_info_card, parent, false);
        return new SubcategoryInfoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Subcategory subcategory = subcategories.get(position);
        if (subcategory != null) {
            holder.nameTextView.setText(subcategory.Name);
            holder.itemView.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.setVisibility(View.GONE);
        }
        holder.deleteButton.setOnClickListener(v -> showDeleteConfirmationDialog(holder, subcategory));


    }

    @Override
    public int getItemCount() {

        return subcategories != null ? subcategories.size() : 0;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;
        MaterialButton deleteButton;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.subcategoryNameTextView);

            deleteButton = itemView.findViewById(R.id.deleteSubcategoryButton);

        }
    }
    public void updateSubcategories(List<Subcategory> newSubcategories) {
        if (this.subcategories == null) {
            this.subcategories = new ArrayList<>();
        }
        this.subcategories.clear();
        if (newSubcategories != null) {
            this.subcategories.addAll(newSubcategories);
        }
        notifyDataSetChanged();
    }

    private void showDeleteConfirmationDialog(SubcategoryInfoAdapter.ViewHolder holder, Subcategory subcategory) {

        new AlertDialog.Builder(context)
                .setTitle("Remove subcategory")
                .setMessage("Are you sure you want to remove this subcategory?")
                .setPositiveButton("Yes", (dialog, which) -> deleteSubcategoryFromFirebase(subcategory))
                .setNegativeButton("No", null)
                .show();
    }

    private void deleteSubcategoryFromFirebase(Subcategory subcategory) {
        new Thread(() -> {
            DatabaseReference subcategoriesRef = FirebaseDatabase.getInstance()
                    .getReference("EventTypes")
                    .child(eventTypeId)
                    .child("suggestedSC");

            subcategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    boolean found = false;

                    for (DataSnapshot child : dataSnapshot.getChildren()) {

                        Subcategory existingSubcategory = child.getValue(Subcategory.class);

                        if (existingSubcategory != null && subcategory.id.equals(existingSubcategory.id)) {

                            child.getRef().removeValue()
                                    .addOnSuccessListener(aVoid -> handler.post(() -> {
                                        Toast.makeText(context, "Suggested subcategory " + subcategory.Name + " removed successfully", Toast.LENGTH_SHORT).show();
                                        subcategories.remove(subcategory);
                                        notifyDataSetChanged();
                                    }))
                                    .addOnFailureListener(e -> handler.post(() -> {
                                        Toast.makeText(context, "Failed to remove subcategory " + subcategory.Name, Toast.LENGTH_SHORT).show();
                                    }));
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        handler.post(() -> {
                            Toast.makeText(context, "Subcategory not found", Toast.LENGTH_SHORT).show();
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    handler.post(() -> {
                        Toast.makeText(context, "Error accessing the database", Toast.LENGTH_SHORT).show();
                    });
                }
            });
        }).start();
    }


}

