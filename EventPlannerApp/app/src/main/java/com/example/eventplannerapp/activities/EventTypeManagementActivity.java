package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplannerapp.adapters.CategoryAdapter;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.R;
import com.example.eventplannerapp.adapters.EventTypeAdapter;
import com.example.eventplannerapp.model.Subcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EventTypeManagementActivity extends BaseActivity {

    private EventTypeAdapter adapter;
    private DatabaseReference databaseEventTypes;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContent(R.layout.activity_event_type_management);
        //setContentView(R.layout.activity_event_type_management);
        databaseEventTypes = FirebaseDatabase.getInstance().getReference("EventTypes");

        RecyclerView recyclerView = findViewById(R.id.etRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new EventTypeAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(adapter);

        loadEventTypesFromFirebase();
    }



    public void AddET(View view) {
        Intent intent = new Intent(this, CreateNewEventTypeActivity.class);
        startActivity(intent);
    }
    private void loadEventTypesFromFirebase() {

        databaseEventTypes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                new EventTypeManagementActivity.ParseEventTypesTask().execute(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(EventTypeManagementActivity.this, "Failed to load event types", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private class ParseEventTypesTask extends AsyncTask<DataSnapshot, Void, List<EventType>> {
        @Override
        protected List<EventType> doInBackground(DataSnapshot... snapshots) {
            List<EventType> eventTypes = new ArrayList<>();
            for (DataSnapshot snapshot : snapshots[0].getChildren()) {
                EventType eventType = snapshot.getValue(EventType.class);
                eventTypes.add(eventType);
            }
            return eventTypes;
        }

        @Override
        protected void onPostExecute(List<EventType> eventTypes) {
            adapter.updateEventTypes(eventTypes);
        }
    }
}