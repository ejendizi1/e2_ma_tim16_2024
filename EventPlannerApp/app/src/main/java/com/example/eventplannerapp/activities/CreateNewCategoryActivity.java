package com.example.eventplannerapp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Category;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CreateNewCategoryActivity extends AppCompatActivity {
    private EditText nameEditText;
    private EditText descriptionEditText;
    private DatabaseReference databaseCategories;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_category);


        databaseCategories = FirebaseDatabase.getInstance().getReference("Categories");

        nameEditText = findViewById(R.id.editTextName);
        descriptionEditText = findViewById(R.id.editTextDescription);
        Button submitButton = findViewById(R.id.buttonSubmit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                executorService.submit(() -> {
                    saveCategory();
                });
            }
        });
    }

    private void saveCategory() {
        String name = nameEditText.getText().toString().trim();
        String description = descriptionEditText.getText().toString().trim();

        if (name.isEmpty() || description.isEmpty()) {
            runOnUiThread(() -> {
                Toast.makeText(CreateNewCategoryActivity.this, "You must enter name and description", Toast.LENGTH_LONG).show();
            });
            return;
        }


        String id = databaseCategories.push().getKey();
        Category category = new Category(id, name, description, new ArrayList<>());

        databaseCategories.child(id).setValue(category).addOnCompleteListener(task -> {

            runOnUiThread(() -> {
                if (task.isSuccessful()) {
                    Toast.makeText(CreateNewCategoryActivity.this, "Category added", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(CreateNewCategoryActivity.this, "Category was not added", Toast.LENGTH_LONG).show();
                }
            });
        });
    }

}
