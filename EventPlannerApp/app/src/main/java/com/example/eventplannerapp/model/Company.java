package com.example.eventplannerapp.model;
import android.media.Image;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Company {

        String id;
        String ownerId;
        String ownerEmail;
        String email;
        String name;
        String description;
        String address;
        String phone;
        String companyPictureUri;

        HashMap<String, WorkDay> workDays;
        List<Category> categories;
        List<EventType> eventTypes;

        public Company() {
            this.workDays = new HashMap<>();
        }

    public Company(String id, String ownerId, String ownerEmail,String email, String name, String description, String address, String phone, String companyPictureUri, HashMap<String, WorkDay> workDays, List<Category> categories, List<EventType> eventTypes) {
        this.id = id;
        this.ownerId = ownerId;
        this.ownerEmail=ownerEmail;
        this.email = email;
        this.name = name;
        this.description = description;
        this.address = address;
        this.phone = phone;
        this.companyPictureUri = companyPictureUri;
        this.workDays = workDays;
        this.categories=categories;
        this.eventTypes=eventTypes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String, WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(HashMap<String, WorkDay> workDays) {
        this.workDays = workDays;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompanyPictureUri() {
        return companyPictureUri;
    }

    public void setCompanyPictureUri(String companyPictureUri) {
        this.companyPictureUri = companyPictureUri;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }
}

