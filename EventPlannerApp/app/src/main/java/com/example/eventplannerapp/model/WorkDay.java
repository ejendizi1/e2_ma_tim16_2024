package com.example.eventplannerapp.model;

import java.io.Serializable;

public class WorkDay implements Serializable {
    private boolean isWorkingDay;
    private String fromTime;
    private String toTime;

    public WorkDay(boolean isWorkingDay, String fromTime, String toTime) {
        this.isWorkingDay = isWorkingDay;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public WorkDay() {
    }

    public boolean isWorkingDay() {
        return isWorkingDay;
    }

    public void setWorkingDay(boolean workingDay) {
        isWorkingDay = workingDay;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

}

