package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.Subcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class SubcategoryEditActivity extends AppCompatActivity {
    private String categoryId, subcategoryId;
    private EditText editTextSubcategoryName, editTextSubcategoryDescription;
    private DatabaseReference subcategoriesRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_subcategory_edit);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Intent intent = getIntent();
        categoryId = intent.getStringExtra("CategoryId");
        subcategoryId = intent.getStringExtra("SubcategoryId");
        String subcategoryName = intent.getStringExtra("SubcategoryName");
        String subcategoryDescription = intent.getStringExtra("SubcategoryDescription");

        editTextSubcategoryName = findViewById(R.id.editTextName);
        editTextSubcategoryDescription = findViewById(R.id.editTextDescription);

        editTextSubcategoryName.setText(subcategoryName);
        editTextSubcategoryDescription.setText(subcategoryDescription);


        subcategoriesRef = FirebaseDatabase.getInstance()
                .getReference("Categories")
                .child(categoryId)
                .child("Subcategories");

        Button updateButton = findViewById(R.id.buttonUpdate);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Extract new values
                String newName = editTextSubcategoryName.getText().toString().trim();
                String newDescription = editTextSubcategoryDescription.getText().toString().trim();

                if (newName.isEmpty() || newDescription.isEmpty()) {
                    Toast.makeText(SubcategoryEditActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                new Thread(() -> {
                    DatabaseReference subcategoriesRef = FirebaseDatabase.getInstance()
                            .getReference("Categories")
                            .child(categoryId)
                            .child("Subcategories");

                    subcategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            boolean found = false;

                            // Locate the matching subcategory by its id field
                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                // Retrieve the subcategory object from the snapshot
                                Subcategory existingSubcategory = child.getValue(Subcategory.class);

                                if (existingSubcategory != null && subcategoryId.equals(existingSubcategory.id)) {
                                    // Create a HashMap for the update
                                    HashMap<String, Object> updatedSubcategory = new HashMap<>();
                                    updatedSubcategory.put("Name", newName);
                                    updatedSubcategory.put("Description", newDescription);

                                    // Perform the update operation asynchronously
                                    child.getRef().updateChildren(updatedSubcategory)
                                            .addOnSuccessListener(aVoid -> runOnUiThread(() -> {
                                                Toast.makeText(SubcategoryEditActivity.this, "Subcategory updated successfully", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }))
                                            .addOnFailureListener(e -> runOnUiThread(() -> {
                                                Toast.makeText(SubcategoryEditActivity.this, "Failed to update subcategory", Toast.LENGTH_SHORT).show();
                                            }));
                                    found = true;
                                    break;
                                }
                            }

                            if (!found) {
                                runOnUiThread(() -> {
                                    Toast.makeText(SubcategoryEditActivity.this, "Subcategory not found", Toast.LENGTH_SHORT).show();
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            runOnUiThread(() -> {
                                Toast.makeText(SubcategoryEditActivity.this, "Error accessing the database", Toast.LENGTH_SHORT).show();
                            });
                        }
                    });
                }).start();
            }
        });
    }

    }