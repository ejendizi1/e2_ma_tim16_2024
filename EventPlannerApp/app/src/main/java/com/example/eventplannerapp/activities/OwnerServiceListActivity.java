package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;

public class OwnerServiceListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_owner_service_list);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void goToPackageListOwner(View view) {
        Intent intent = new Intent(this, OwnerPackageListActivity.class);
        startActivity(intent);
    }

    public void goToServiceListOwner(View view) {
        Intent intent = new Intent(this, OwnerServiceListActivity.class);
        startActivity(intent);
    }

    public void goToProductListOwner(View view) {
        Intent intent = new Intent(this, OwnerProductListActivity.class);
        startActivity(intent);
    }

    public void goToServiceFilterOwner(View view) {
        Intent intent = new Intent(this, OwnerServiceFilterActivity.class);
        startActivity(intent);
    }
}