package com.example.eventplannerapp.model;

public enum SubcategoryType {
    SERVICE,
    PRODUCT
}
