package com.example.eventplannerapp.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.fragments.ServiceProviderFragment1;
import com.example.eventplannerapp.model.ServiceProviderRegistrationViewModel;

public class ServiceProviderActivity extends AppCompatActivity {

    private ServiceProviderRegistrationViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider);

        viewModel = new ViewModelProvider(this).get(ServiceProviderRegistrationViewModel.class);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new ServiceProviderFragment1())
                .commit();
    }

    public ServiceProviderRegistrationViewModel getViewModel() {
        return viewModel;
    }

}
