package com.example.eventplannerapp.adapters;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.CategoryEditActivity;
import com.example.eventplannerapp.activities.CreateNewSubcategoryActivity;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.Subcategory;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private List<Category> categories;
    private Context context;
    private LayoutInflater inflater;
    private static final ExecutorService executorService = Executors.newFixedThreadPool(3);


    public CategoryAdapter(List<Category> dataSet, Context context) {
        this.categories = dataSet;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.nameTW.setText(category.Name);
        holder.descriptionTW.setText(category.Description);
        executorService.submit(() -> {
            List<Subcategory> subcategories = (category.Subcategories != null) ? category.Subcategories : new ArrayList<>();

            // Update the RecyclerView adapter on the main thread
            holder.subcategoryRV.post(() -> {
                if (holder.subcategoryAdapter == null) {
                    holder.subcategoryAdapter = new SubcategoryAdapter(subcategories, category.id, context);
                    holder.subcategoryRV.setAdapter(holder.subcategoryAdapter);
                } else {
                    holder.subcategoryAdapter.updateSubcategories(subcategories);
                }
            });
        });



        holder.addSubcategoryButton.setOnClickListener(v -> {
            Intent intent = new Intent(context, CreateNewSubcategoryActivity.class);
            intent.putExtra("CategoryId", category.id);
            context.startActivity(intent);
        });
        holder.editButton.setOnClickListener(v -> {
            Intent intent = new Intent(context, CategoryEditActivity.class);
            intent.putExtra("CategoryId", category.id);
            intent.putExtra("CategoryName", category.Name);
            intent.putExtra("CategoryDescription", category.Description);
            context.startActivity(intent);
        });

        holder.deleteButton.setOnClickListener(v -> showDeleteConfirmationDialog(holder, category));

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTW, descriptionTW;
        MaterialButton addSubcategoryButton, editButton, deleteButton;
        RecyclerView subcategoryRV;
        SubcategoryAdapter subcategoryAdapter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTW = itemView.findViewById(R.id.categoryNameTextView);
            descriptionTW = itemView.findViewById(R.id.categoryDescriptionTextView);
            addSubcategoryButton = itemView.findViewById(R.id.addSubcategoryButton);
            editButton = itemView.findViewById(R.id.editCategoryButton);
            deleteButton = itemView.findViewById(R.id.deleteCategoryButton);
            subcategoryRV = itemView.findViewById(R.id.subcategoryRecyclerView);

            subcategoryRV.setLayoutManager(new LinearLayoutManager(itemView.getContext()));

        }
    }

    public void updateCategories(List<Category> newCategories) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return categories.size();
            }

            @Override
            public int getNewListSize() {
                return newCategories.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return categories.get(oldItemPosition).id.equals(newCategories.get(newItemPosition).id);
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return categories.get(oldItemPosition).equals(newCategories.get(newItemPosition));
            }
        });

        categories.clear();
        categories.addAll(newCategories);
        diffResult.dispatchUpdatesTo(this);
    }


    private void showDeleteConfirmationDialog(ViewHolder holder, Category category) {

        new AlertDialog.Builder(context)
                .setTitle("Delete Category")
                .setMessage("Are you sure you want to delete this category?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        deleteCategoryFromFirebase(category);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void deleteCategoryFromFirebase(Category category) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Categories").child(category.getId());


        databaseReference.removeValue().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Toast.makeText(context, "Category " + category.Name + " deleted successfully", Toast.LENGTH_SHORT).show();
                categories.remove(category);
                notifyDataSetChanged();
            } else {
                Toast.makeText(context, "Failed to delete category " + category.Name, Toast.LENGTH_SHORT).show();
            }
        });
    }
}