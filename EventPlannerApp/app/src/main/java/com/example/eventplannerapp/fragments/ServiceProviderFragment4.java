package com.example.eventplannerapp.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.ServiceProviderActivity;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.Company;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.model.ServiceProviderRegistrationViewModel;
import com.example.eventplannerapp.model.Subcategory;
import com.example.eventplannerapp.model.User;
import com.example.eventplannerapp.model.UserRequest;
import com.example.eventplannerapp.model.WorkDay;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceProviderFragment4#newInstance} factory method to
 * create an instance of this fragment.
 */
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServiceProviderFragment4 extends Fragment {

    private ServiceProviderRegistrationViewModel viewModel;
    FirebaseAuth mAuth;
    DatabaseReference usersDbRef;
    private List<Category> selectedCategories = new ArrayList<>();
    private Map<String, Category> categoryMap = new HashMap<>();
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private Map<String, EventType> eventTypeMap = new HashMap<>();
    String fcmToken;


    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public ServiceProviderFragment4() {
    }

    public static ServiceProviderFragment4 newInstance() {
        return new ServiceProviderFragment4();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ((ServiceProviderActivity) getActivity()).getViewModel();
        }
        mAuth = FirebaseAuth.getInstance();
        usersDbRef = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_service_provider4, container, false);
        Button buttonRegister = rootView.findViewById(R.id.buttonRegister);
        Spinner categorySpinner = rootView.findViewById(R.id.categorySpinner);
        Button addCategoryButton = rootView.findViewById(R.id.addCategoryButton);
        ListView categoryListView = rootView.findViewById(R.id.categoryListView);
        List<String> initialCategories = new ArrayList<>();
        ArrayAdapter<String> categoryListAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, initialCategories);
        categoryListView.setAdapter(categoryListAdapter);

        Spinner eventTypeSpinner = rootView.findViewById(R.id.eventTypeSpinner);
        Button addEventTypeButton = rootView.findViewById(R.id.addEventTypeButton);
        ListView eventTypeListView = rootView.findViewById(R.id.eventTypeListView);
        List<String> initialEventTypes = new ArrayList<>();
        ArrayAdapter<String> eventTypeListAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, initialEventTypes);
        eventTypeListView.setAdapter(eventTypeListAdapter);

        executorService.submit(() -> loadCategories(categorySpinner));
        executorService.submit(() -> loadEventTypes(eventTypeSpinner));

        // Add Category Button logic
        addCategoryButton.setOnClickListener(v -> {
            String selectedCategoryName = (String) categorySpinner.getSelectedItem();
            if (selectedCategoryName != null) {
                ArrayAdapter<String> categoryListAdapter1 = (ArrayAdapter<String>) categoryListView.getAdapter();
                categoryListAdapter1.add(selectedCategoryName);
                categoryListAdapter1.notifyDataSetChanged();

                Category selectedCategory = categoryMap.get(selectedCategoryName);
                selectedCategories.add(selectedCategory);

                ArrayAdapter<String> spinnerAdapter = (ArrayAdapter<String>) categorySpinner.getAdapter();
                spinnerAdapter.remove(selectedCategoryName);
                spinnerAdapter.notifyDataSetChanged();
            }
        });

        // Add Event Type Button logic
        addEventTypeButton.setOnClickListener(v -> {
            String selectedETName = (String) eventTypeSpinner.getSelectedItem();
            if (selectedETName != null) {
                ArrayAdapter<String> eventTypeListAdapter1 = (ArrayAdapter<String>) eventTypeListView.getAdapter();
                eventTypeListAdapter1.add(selectedETName);
                eventTypeListAdapter1.notifyDataSetChanged();

                EventType selectedEventType = eventTypeMap.get(selectedETName);
                selectedEventTypes.add(selectedEventType);

                ArrayAdapter<String> spinnerAdapter1 = (ArrayAdapter<String>) eventTypeSpinner.getAdapter();
                spinnerAdapter1.remove(selectedETName);
                spinnerAdapter1.notifyDataSetChanged();
            }
        });

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email, password, firstName, lastName, address, phone, profilePictureUri, role;
                Date registered = new Date();
                HashMap<String, WorkDay> wH = viewModel.getWorkDays();
                Boolean isAdminVerified = false;
                email = String.valueOf(viewModel.getEmail());
                password = String.valueOf(viewModel.getConfirmPassword());
                firstName = String.valueOf(viewModel.getFirstName());
                lastName = String.valueOf(viewModel.getLastName());
                address = String.valueOf(viewModel.getAddress());
                phone = String.valueOf(viewModel.getPhone());
                profilePictureUri = String.valueOf(viewModel.getProfilePictureUri());
                role = "SPO";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar calendar = Calendar.getInstance();
                Date futureDate = calendar.getTime();
                String futureDateTime = sdf.format(futureDate);
                String emailC = String.valueOf(viewModel.getCompanyEmail());
                String nameC = String.valueOf(viewModel.getCompanyName());
                String descriptionC = String.valueOf(viewModel.getCompanyDescription());
                String addressC = String.valueOf(viewModel.getCompanyAddress());
                String phoneC = String.valueOf(viewModel.getCompanyPhone());
                String companyPictureUri = String.valueOf(viewModel.getCompanyPictureUri());

                FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("not", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String fcmToken = task.getResult().toString();

                        // Log and toast
                        Log.d("not", fcmToken);

                        String uid = FirebaseDatabase.getInstance().getReference("UsersRequest").push().getKey();
                        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("UsersRequest").child(uid);
                        UserRequest user = new UserRequest(uid, email, firstName, lastName, address, phone, role, profilePictureUri, isAdminVerified, futureDateTime, password, emailC, nameC, addressC, phoneC, descriptionC, companyPictureUri, wH, selectedCategories, selectedEventTypes, fcmToken);
                        userRef.setValue(user);
                        /* userRef.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(requireContext(), "User registered successfully", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(requireContext(), "Failed to register user", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });*/
                    }
                });



                /*DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies").push();
                String companyId = companyRef.getKey();
                Company company = new Company(companyId, ownerId,email, emailC, nameC, descriptionC, addressC, phoneC, companyPictureUri, wH,selectedCategories,selectedEventTypes);
                companyRef.setValue(company);
                Log.d("KRAJ KOMP id",companyId);
                Log.d("KRAJ KOMP owner",ownerId);
                Log.d("KRAJ KOMP email",email);
                Log.d("KRAJ KOMP d",descriptionC);
                Log.d("KRAJ KOMP a",addressC);
                Log.d("KRAJ KOMP p",phoneC);
                Log.d("KRAJ KOMP pu",companyPictureUri);
                Log.d("KRAJ KOMP wh", String.valueOf(wH));*/

                Toast.makeText(requireContext(), "Account created.Wait for approve.", Toast.LENGTH_SHORT).show();
                if (getActivity() != null) {
                    getActivity().finish();
                }

            }


        });


                return rootView;
    }

    // Load categories in a separate thread
    private void loadCategories(Spinner categorySpinner) {
        DatabaseReference categoriesRef = FirebaseDatabase.getInstance().getReference("Categories");
        categoriesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<String> categoryNames = new ArrayList<>();
                categoryMap.clear();

                for (DataSnapshot categorySnapshot : snapshot.getChildren()) {
                    Category category = categorySnapshot.getValue(Category.class);
                    if (category != null) {
                        categoryMap.put(category.Name, category);
                        categoryNames.add(category.Name);
                    }
                }

                // Update the spinner adapter on the main UI thread
                getActivity().runOnUiThread(() -> {
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categorySpinner.setAdapter(spinnerAdapter);
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("DatabaseError", "Error loading categories", error.toException());
            }
        });
    }

    // Load event types in a separate thread
    private void loadEventTypes(Spinner eventTypeSpinner) {
        DatabaseReference eventTypesRef = FirebaseDatabase.getInstance().getReference("EventTypes");
        eventTypesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<String> eventTypeNames = new ArrayList<>();
                eventTypeMap.clear();

                for (DataSnapshot eventTypeSnapshot : snapshot.getChildren()) {
                    EventType eventType = eventTypeSnapshot.getValue(EventType.class);
                    if (eventType != null) {
                        eventTypeMap.put(eventType.name, eventType);
                        eventTypeNames.add(eventType.name);
                    }
                }

                // Update the spinner adapter on the main UI thread
                getActivity().runOnUiThread(() -> {
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    eventTypeSpinner.setAdapter(spinnerAdapter);
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("DatabaseError", "Error loading event types", error.toException());
            }
        });
    }
}
