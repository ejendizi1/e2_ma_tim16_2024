package com.example.eventplannerapp.model;

import java.io.Serializable;
import java.util.List;

public class Category implements Serializable {
    public String id;
    public String Name;
    public String Description;
    public List<Subcategory> Subcategories;
    public Category() {
    }


    public Category(String id, String name, String description, List<Subcategory> subcategories) {
        this.id = id;
        this.Name = name;
        this.Description = description;
        this.Subcategories = subcategories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
