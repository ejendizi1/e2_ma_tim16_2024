package com.example.eventplannerapp.model;

import android.media.Image;

import java.util.Date;

public class User {
    String email;
    String firstName;
    String lastName;
    String address;
    String phone;
    String role;
    boolean isEmailVerified;

    String emailSentTime;


    private String profilePictureUri;
    String fcmToken;

    public User() {
    }

    public User(String email, String firstName, String lastName, String address, String phone, String role, String profilePictureUri, boolean isEmailVerified, String emailSentTime,String fcmToken) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.role = role;
        this.isEmailVerified = isEmailVerified;
        this.profilePictureUri = profilePictureUri;
        this.emailSentTime = emailSentTime;
        this.fcmToken=fcmToken;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfilePictureUri() {
        return profilePictureUri;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getRole() {
        return role;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setProfilePictureUri(String profilePictureUri) {
        this.profilePictureUri = profilePictureUri;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(String role) {
        this.role = role;
    }
    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getEmailSentTime() {
        return emailSentTime;
    }

    public void setEmailSentTime(String emailSentTime) {
        this.emailSentTime = emailSentTime;
    }
}
