package com.example.eventplannerapp.model;

import java.io.Serializable;

public class SubcategoryRequest{
    public String id;
    boolean isAdminVerified;
    String categoryId;
    String CategoryName;
    String Name;
    String Description;
    SubcategoryType Type;

    public SubcategoryRequest() {
    }

    public SubcategoryRequest(String id,String categoryId, String categoryName, String name, String description, SubcategoryType type,Boolean isAdminVerified) {
        this.id=id;
        this.categoryId = categoryId;
        this.CategoryName = categoryName;
        Name = name;
        Description = description;
        Type = type;
        this.isAdminVerified = isAdminVerified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isAdminVerified() {
        return isAdminVerified;
    }

    public void setAdminVerified(boolean isAdminVerified) {
        this.isAdminVerified = isAdminVerified;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        this.CategoryName = categoryName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public SubcategoryType getType() {
        return Type;
    }

    public void setType(SubcategoryType type) {
        Type = type;
    }
}
