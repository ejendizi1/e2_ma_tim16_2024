package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterOdActivity extends AppCompatActivity {

    private ActivityResultLauncher<String> mGetContent;
    private ImageView mImageViewSelectedPicture;
    EditText editTextEmail,editTextFirstPassword,editTextPassword,editTextFirstName,editTextLastName,editTextAddress,editTextPhone;
    Button registrationButton;
    FirebaseAuth mAuth;
    TextView loginNow;
    String fcmToken;
    DatabaseReference usersDbRef;
    String profilePictureUri;
    Boolean isEmailVerified;


    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_od);
        mAuth=FirebaseAuth.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        editTextEmail=findViewById(R.id.editTextEmail);
        editTextPassword=findViewById(R.id.editTextConfirmPassword);
        editTextFirstPassword=findViewById(R.id.editTextPassword);
        registrationButton=findViewById(R.id.buttonRegister);
        loginNow=findViewById(R.id.textViewNowLogin);
        editTextFirstName=findViewById(R.id.editTextFirstName);
        editTextLastName=findViewById(R.id.editTextLastName);
        editTextAddress=findViewById(R.id.editTextAddress);
        editTextPhone=findViewById(R.id.editTextPhoneNumber);
        usersDbRef= FirebaseDatabase.getInstance().getReference().child("Users");


        Button uploadButton = findViewById(R.id.buttonUploadProfilePicture);
        mImageViewSelectedPicture = findViewById(R.id.imageViewSelectedPicture);

        uploadButton.setOnClickListener(v -> openGallery());


        mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {

                    mImageViewSelectedPicture.setVisibility(View.VISIBLE);
                    mImageViewSelectedPicture.setImageURI(uri);
                    profilePictureUri=uri.toString();
                });

        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageViewSelectedPicture.setVisibility(View.GONE);
                String email,firstPassword,password,firstName,lastName,address,phone,role;
                email=String.valueOf(editTextEmail.getText());
                firstPassword=String.valueOf(editTextFirstPassword.getText());
                password=String.valueOf(editTextPassword.getText());
                firstName=String.valueOf(editTextFirstName.getText());
                lastName=String.valueOf(editTextLastName.getText());
                address=String.valueOf(editTextAddress.getText());
                phone=String.valueOf(editTextPhone.getText());
                isEmailVerified=false;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentDateTime = sdf.format(new Date());
                role="EO";


                if(!TextUtils.equals(password, firstPassword)){
                    Toast.makeText(RegisterOdActivity.this,"Passwords do not match",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (profilePictureUri == null) {
                    profilePictureUri="";
                }


                if(TextUtils.isEmpty(email)){
                    Toast.makeText(RegisterOdActivity.this,"Enter email",Toast.LENGTH_SHORT).show();
                return;
                }

                if(TextUtils.isEmpty(firstName)){
                    Toast.makeText(RegisterOdActivity.this,"Enter first name",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(lastName)){
                    Toast.makeText(RegisterOdActivity.this,"Enter last name",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(address)){
                    Toast.makeText(RegisterOdActivity.this,"Enter address",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(phone)){
                    Toast.makeText(RegisterOdActivity.this,"Enter phone",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    Toast.makeText(RegisterOdActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                    return;
                }
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    Log.w("not", "Fetching FCM registration token failed", task.getException());
                                    return;
                                }

                                // Get new FCM registration token
                                fcmToken = task.getResult();

                                // Log and toast

                                Log.d("not", fcmToken);
                            }
                        });

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    FirebaseUser firebaseUser = mAuth.getCurrentUser();
                                    if (firebaseUser != null) {
                                        String uid = firebaseUser.getUid();
                                        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(uid);
                                        User user = new User(email, firstName, lastName, address, phone, role, profilePictureUri, isEmailVerified,currentDateTime,fcmToken);
                                        userRef.setValue(user);
                                    }
                                    sendVerificationEmail();
                                    Toast.makeText(RegisterOdActivity.this, "Account created. Please check your email for verification link.", Toast.LENGTH_LONG).show();

                                } else {
                                    Toast.makeText(RegisterOdActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();

                                }
                            }
                        });

            }
        });
    }


    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        mGetContent.launch("image/*");

    }

    private void sendVerificationEmail() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                               } else {
                                Toast.makeText(RegisterOdActivity.this, "Failed to send verification email.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    public void loginNow(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
