package com.example.eventplannerapp.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.adapters.UserRequestAdapter;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.model.UserRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UserManagementActivity extends BaseActivity {
    private static final int PERMISSION_REQUEST_CODE = 100;
    private RecyclerView recyclerView;
    private UserRequestAdapter userAdapter;
    private ArrayList<UserRequest> userList;
    private ArrayList<UserRequest> filteredList;
    private DatabaseReference usersDbRef;
    private Spinner sortSpinner;
    private Spinner categorySpinner;
    private Spinner eventTypeSpinner;
    private EditText searchEditText;
    private ExecutorService executorService;
    private ArrayList<String> categoryList;
    private ArrayList<String> eventTypeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_user_management);
        setActivityContent(R.layout.activity_user_management);

        executorService = Executors.newSingleThreadExecutor();


        recyclerView = findViewById(R.id.etRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        userList = new ArrayList<>();
        filteredList = new ArrayList<>();
        userAdapter = new UserRequestAdapter(filteredList, this);
        recyclerView.setAdapter(userAdapter);

        usersDbRef = FirebaseDatabase.getInstance().getReference().child("UsersRequest");
        searchEditText = findViewById(R.id.searchEditText);
/*
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
        } else {
            fetchUserData();
        }*/
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        usersDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    UserRequest user = dataSnapshot.getValue(UserRequest.class);
                    if (user != null && user.getRole().equals("SPO") && !user.isAdminVerified()) {
                        userList.add(user);
                    }
                }
                filter();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(UserManagementActivity.this, "Failed to fetch data.", Toast.LENGTH_SHORT).show();
            }
        });

        sortSpinner = findViewById(R.id.sortSpinner);
        categorySpinner = findViewById(R.id.categorySpinner);
        eventTypeSpinner = findViewById(R.id.eventTypeSpinner);

        categoryList = new ArrayList<>();
        eventTypeList = new ArrayList<>();

        setupSpinners();
        setupFilters();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sort_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(adapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortUserList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void setupSpinners() {
        DatabaseReference categoryDbRef = FirebaseDatabase.getInstance().getReference().child("Categories");
        categoryDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                categoryList.clear();
                categoryList.add("All categories");
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Category category = dataSnapshot.getValue(Category.class);
                    if (category != null && category.Name != null) {
                        categoryList.add(category.Name);
                    }
                }
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(UserManagementActivity.this, android.R.layout.simple_spinner_item, categoryList);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(UserManagementActivity.this, "Failed to fetch categories.", Toast.LENGTH_SHORT).show();
            }
        });

        DatabaseReference eventTypeDbRef = FirebaseDatabase.getInstance().getReference().child("EventTypes");
        eventTypeDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                eventTypeList.clear();
                eventTypeList.add("All event types");
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    EventType eventType = dataSnapshot.getValue(EventType.class);
                    if (eventType != null && eventType.name != null) {
                        eventTypeList.add(eventType.name);
                    }
                }
                ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(UserManagementActivity.this, android.R.layout.simple_spinner_item, eventTypeList);
                eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventTypeSpinner.setAdapter(eventTypeAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(UserManagementActivity.this, "Failed to fetch event types.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupFilters() {
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void sortUserList() {
        executorService.submit(() -> {
            String selectedOption = sortSpinner.getSelectedItem().toString();
            if (selectedOption.equals("Sort by Date Ascending")) {
                Collections.sort(filteredList, new Comparator<UserRequest>() {
                    @Override
                    public int compare(UserRequest u1, UserRequest u2) {
                        return compareDates(u1.getRequestSentTime(), u2.getRequestSentTime());
                    }
                });
            } else if (selectedOption.equals("Sort by Date Descending")) {
                Collections.sort(filteredList, new Comparator<UserRequest>() {
                    @Override
                    public int compare(UserRequest u1, UserRequest u2) {
                        return compareDates(u2.getRequestSentTime(), u1.getRequestSentTime());
                    }
                });
            }
            runOnUiThread(() -> userAdapter.notifyDataSetChanged());
        });
    }

    private int compareDates(String date1, String date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d1 = sdf.parse(date1);
            Date d2 = sdf.parse(date2);
            return d1.compareTo(d2);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void filter() {
        executorService.submit(() -> {
            filteredList.clear();
            String selectedCategory = categorySpinner.getSelectedItem() != null ? categorySpinner.getSelectedItem().toString() : "";
            String selectedEventType = eventTypeSpinner.getSelectedItem() != null ? eventTypeSpinner.getSelectedItem().toString() : "";
            String searchText = searchEditText.getText().toString().toLowerCase();
            boolean filterByCategory = !selectedCategory.equalsIgnoreCase("All categories");
            boolean filterByEventType = !selectedEventType.equalsIgnoreCase("All event types");

            for (UserRequest user : userList) {
                boolean matchesSearchText = user.getFirstName().toLowerCase().contains(searchText) ||
                        user.getLastName().toLowerCase().contains(searchText) ||
                        user.getEmail().toLowerCase().contains(searchText) ||
                        user.getCompanyName().toLowerCase().contains(searchText) ||
                        user.getCompanyEmail().toLowerCase().contains(searchText);

                boolean matchesCategory = !filterByCategory || (user.getCategories() != null && user.getCategories().stream()
                        .anyMatch(category -> category.Name.equalsIgnoreCase(selectedCategory)));
                boolean matchesEventType = !filterByEventType || (user.getEventTypes() != null && user.getEventTypes().stream()
                        .anyMatch(eventType -> eventType.name.equalsIgnoreCase(selectedEventType)));

                if (matchesSearchText && (selectedCategory.isEmpty() || matchesCategory) && (selectedEventType.isEmpty() || matchesEventType)) {
                    filteredList.add(user);
                }
            }
            runOnUiThread(() -> sortUserList());
        });
    }

/*
    private void fetchUserData() {
        usersDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                userList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    UserRequest user = dataSnapshot.getValue(UserRequest.class);
                    if (user != null && user.getRole().equals("SPO") && !user.isAdminVerified()) {
                        userList.add(user);
                    }
                }
                userAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(UserManagementActivity.this, "Failed to fetch data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchUserData();
            } else {
                Toast.makeText(this, "Permission denied.", Toast.LENGTH_SHORT).show();
            }
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (executorService != null && !executorService.isShutdown()) {
            executorService.shutdown();
        }
    }
}
