package com.example.eventplannerapp.model;

import java.io.Serializable;

public class Subcategory implements Serializable {
    public String id;
    public String Name;
    public String Description;
    public SubcategoryType Type;

    public Subcategory() {
    }

    public Subcategory(String id,String name, String description, SubcategoryType type) {
        this.id=id;
        this.Name = name;
        this.Description = description;
        this.Type = type;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
