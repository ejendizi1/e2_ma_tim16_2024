package com.example.eventplannerapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.activities.ServiceProviderActivity;
import com.example.eventplannerapp.model.ServiceProviderRegistrationViewModel;


public class ServiceProviderFragment1 extends Fragment {
    private EditText editTextEmail;
    private EditText editTextFirstPassword;
    private EditText editTextPassword;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextAddress;
    private EditText editTextPhone;
    private ActivityResultLauncher<String> mGetContent;
    private ImageView mImageViewSelectedPicture;

    private ServiceProviderRegistrationViewModel viewModel;
    public ServiceProviderFragment1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ((ServiceProviderActivity) requireActivity()).getViewModel();


    }
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_service_provider1, container, false);

        // Initialize views
        Button buttonNext = rootView.findViewById(R.id.buttonNext);


        editTextEmail = rootView.findViewById(R.id.editTextEdit);
        editTextFirstPassword = rootView.findViewById(R.id.editTextPassword);
        editTextPassword = rootView.findViewById(R.id.editTextConfirmPassword);
        editTextFirstName = rootView.findViewById(R.id.editTextFirstName);
        editTextLastName = rootView.findViewById(R.id.editTextLastName);
        editTextAddress = rootView.findViewById(R.id.editTextAddress);
        editTextPhone = rootView.findViewById(R.id.editTextPhoneNumber);


        // Postavljanje unesenih podataka iz ViewModel-a u polja za unos
        editTextEmail.setText(viewModel.getEmail() != null ? viewModel.getEmail() : "");
        editTextFirstPassword.setText(viewModel.getPassword() != null ? viewModel.getPassword() : "");
        editTextPassword.setText(viewModel.getConfirmPassword() != null ? viewModel.getConfirmPassword() : "");
        editTextFirstName.setText(viewModel.getFirstName() != null ? viewModel.getFirstName() : "");
        editTextLastName.setText(viewModel.getLastName() != null ? viewModel.getLastName() : "");
        editTextAddress.setText(viewModel.getAddress() != null ? viewModel.getAddress() : "");
        editTextPhone.setText(viewModel.getPhone() != null ? viewModel.getPhone() : "");
        Button uploadButton = rootView.findViewById(R.id.buttonUploadProfilePicture);
        mImageViewSelectedPicture = rootView.findViewById(R.id.imageViewSelectedPicture);

        uploadButton.setOnClickListener(v -> openGallery());


        mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                uri -> {
                    mImageViewSelectedPicture.setVisibility(View.VISIBLE);
                    mImageViewSelectedPicture.setImageURI(uri);
                    viewModel.setProfilePictureUri(uri.toString());
                });



        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextFirstPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setConfirmPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setFirstName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setLastName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setAddress(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTextPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.setPhone(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fieldsValid = validateFields();
                boolean passwordsMatch = validatePasswords();
                if (viewModel.getProfilePictureUri() == null) {
                    viewModel.setProfilePictureUri("");
                }

                if (fieldsValid && passwordsMatch) {
                    FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, new ServiceProviderFragment2())
                            .addToBackStack(null)
                            .commit();
                } else {
                    if (!fieldsValid) {
                        Toast.makeText(requireContext(), "All fields are required.", Toast.LENGTH_SHORT).show();
                    }
                    if (!passwordsMatch) {
                        Toast.makeText(requireContext(), "Passwords do not match.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



        return rootView;
    }

    private boolean validatePasswords() {
        String password = editTextFirstPassword.getText().toString();
        String confirmPassword = editTextPassword.getText().toString();

        return password.equals(confirmPassword);
    }
    private boolean validateFields() {
        String email = editTextEmail.getText().toString();
        String password = editTextFirstPassword.getText().toString();
        String confirmPassword = editTextPassword.getText().toString();
        String firstName = editTextFirstName.getText().toString();
        String lastName = editTextLastName.getText().toString();
        String address = editTextAddress.getText().toString();
        String phone = editTextPhone.getText().toString();

        if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() || firstName.isEmpty()
                || lastName.isEmpty() || address.isEmpty() || phone.isEmpty()) {
            return false;
        }

        return true;
    }

    private void openGallery() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                mGetContent.launch("image/*");
            }
        });
        thread.start();
    }


}
