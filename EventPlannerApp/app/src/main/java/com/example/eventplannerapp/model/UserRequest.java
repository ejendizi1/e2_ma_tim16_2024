package com.example.eventplannerapp.model;

import android.media.Image;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UserRequest {
    public String id;
    String email;
    String firstName;
    String lastName;
    String address;
    String phone;
    String role;
    boolean isAdminVerified;
    String fcmToken;
    String requestSentTime;

    private String profilePictureUri;
    String password;

    //za kompaniju deo

    String companyEmail;
    String companyName;
    String companyAddress;
    String companyPhone;
    String companyDescription;
    String companyPictureUri;
    HashMap<String, WorkDay> workDays;
    List<Category> categories;
    List<EventType> eventTypes;

    public UserRequest() {
    }

    public UserRequest(String id,String email, String firstName, String lastName, String address, String phone, String role, String profilePictureUri, boolean isAdminVerified, String requestSentTime,String password,String companyEmail, String companyName, String companyAddress, String companyPhone, String companyDescription, String companyPictureUri, HashMap<String, WorkDay> workDays,List<Category>categories,List<EventType> eventTypes,String fcmToken) {
        this.id=id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.role = role;
        this.isAdminVerified = isAdminVerified;
        this.profilePictureUri = profilePictureUri;
        this.requestSentTime = requestSentTime;
        this.isAdminVerified = isAdminVerified;
        this.password=password;

        this.companyEmail = companyEmail;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.companyDescription = companyDescription;
        this.companyPictureUri = companyPictureUri;
        this.workDays = workDays;
        this.categories=categories;
        this.eventTypes=eventTypes;
        this.fcmToken=fcmToken;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfilePictureUri() {
        return profilePictureUri;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getRole() {
        return role;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setProfilePictureUri(String profilePictureUri) {
        this.profilePictureUri = profilePictureUri;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isAdminVerified() {
        return isAdminVerified;
    }

    public void setAdminVerified(boolean isAdminVerified) {
        this.isAdminVerified = isAdminVerified;
    }

    public String getRequestSentTime() {
        return requestSentTime;
    }

    public void setRequestSentTime(String emailSentTime) {
        this.requestSentTime = emailSentTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getCompanyPictureUri() {
        return companyPictureUri;
    }

    public void setCompanyPictureUri(String companyPictureUri) {
        this.companyPictureUri = companyPictureUri;
    }

    public HashMap<String, WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(HashMap<String, WorkDay> workDays) {
        this.workDays = workDays;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
