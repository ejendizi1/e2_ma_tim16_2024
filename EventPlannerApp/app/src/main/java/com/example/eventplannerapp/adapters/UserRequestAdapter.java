package com.example.eventplannerapp.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.eventplannerapp.R;
import com.example.eventplannerapp.SendMailTask;
import com.example.eventplannerapp.model.Category;
import com.example.eventplannerapp.model.Company;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.model.User;
import com.example.eventplannerapp.model.UserRequest;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UserRequestAdapter extends RecyclerView.Adapter<UserRequestAdapter.UserRequestViewHolder> {

    private ArrayList<UserRequest> userList;
    private Context context;
    private FirebaseAuth mAuth;
    private Executor executor,executor1;



    public UserRequestAdapter(ArrayList<UserRequest> userList, Context context) {
        this.userList = userList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
        executor = Executors.newSingleThreadExecutor();
        executor1 = Executors.newSingleThreadExecutor();
    }

    @NonNull
    @Override
    public UserRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_card, parent, false);
        return new UserRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserRequestViewHolder holder, int position) {
        UserRequest user = userList.get(position);
        holder.emailTextView.setText(user.getEmail());
        holder.nameTextView.setText(user.getFirstName());
        holder.lastNameTextView.setText(user.getLastName());





        holder.acceptButton.setOnClickListener(v -> executor.execute(() -> acceptUserRequest(user)));
        holder.declineButton.setOnClickListener(v -> showDeclineReasonDialog(user));
        holder.detailsButton.setOnClickListener(v -> showDetailsDialog(user));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class UserRequestViewHolder extends RecyclerView.ViewHolder {
        TextView emailTextView, nameTextView, lastNameTextView, addressTextView, phoneTextView;
        ImageView profileImageView;
        MaterialButton acceptButton,declineButton,detailsButton;

        public UserRequestViewHolder(@NonNull View itemView) {
            super(itemView);
            emailTextView = itemView.findViewById(R.id.emailTextView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            lastNameTextView = itemView.findViewById(R.id.lastNameTextView);
            acceptButton = itemView.findViewById(R.id.acceptButton);
            declineButton = itemView.findViewById(R.id.declineButton);
            detailsButton = itemView.findViewById(R.id.detailsButton);
        }
    }

    public void filterList(ArrayList<UserRequest> filteredList) {
        userList = filteredList;
        notifyDataSetChanged();
    }

    private void acceptUserRequest(UserRequest user) {
        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = task.getResult().getUser();
                        String uid = firebaseUser.getUid();

                        firebaseUser.sendEmailVerification()
                                .addOnCompleteListener(emailTask -> {
                                    if (emailTask.isSuccessful()) {
                                        ((Activity) context).runOnUiThread(() ->
                                                Toast.makeText(context, "Verification email sent", Toast.LENGTH_SHORT).show()
                                        );
                                        updateUserRegistrationStatus(user);

                                        boolean isEmailVerified=false;
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Calendar calendar = Calendar.getInstance();
                                        Date futureDate = calendar.getTime();
                                        String futureDateTime = sdf.format(futureDate);


                                        String ownerId = uid;
                                        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(uid);
                                        User userValid = new User(user.getEmail(), user.getFirstName(), user.getLastName(), user.getAddress(), user.getPhone(), user.getRole(), user.getProfilePictureUri(), isEmailVerified,futureDateTime,user.getFcmToken());
                                        userRef.setValue(userValid);



                                        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies").push();
                                        String companyId = companyRef.getKey();
                                        Company company = new Company(companyId, ownerId,user.getEmail(), user.getCompanyEmail(), user.getCompanyName(), user.getCompanyDescription(), user.getCompanyAddress(), user.getCompanyPhone(), user.getCompanyPictureUri(), user.getWorkDays(),user.getCategories(),user.getEventTypes());
                                        companyRef.setValue(company);

                                    } else {
                                        ((Activity) context).runOnUiThread(() ->
                                                Toast.makeText(context, "Failed to send verification email", Toast.LENGTH_SHORT).show()
                                        );
                                    }
                                });
                    } else {
                        ((Activity) context).runOnUiThread(() ->
                                Toast.makeText(context, "Failed! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show()
                        );
                    }
                });
    }
    private void showDeclineReasonDialog(UserRequest user) {
        ((Activity) context).runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Reason for rejection:");


            final EditText input = new EditText(context);
            input.setHint("Enter reason:");
            builder.setView(input);


            builder.setPositiveButton("OK", (dialog, which) -> {
                String reason = input.getText().toString();
                executor1.execute(() -> declineUserRequest(user, reason));
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

            builder.show();
        });
    }
    private void declineUserRequest(UserRequest user,String reason) {

        SendMailTask sendMailTask = new SendMailTask(context, user.getEmail(), "Odbijen zahtev za registraciju", reason);
        sendMailTask.execute();
        updateUserRegistrationStatus(user);


    }
    private void showDetailsDialog(UserRequest user) {
        ((Activity) context).runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Details about request:");

            StringBuilder details = new StringBuilder();
            details.append("<i>Request sent:</i> ").append("<i>").append(user.getRequestSentTime()).append("</i>").append("<br>");
            details.append("<b>About user:</b> ").append("<br>").append("<br>");
            details.append("User email: ").append(user.getEmail()).append("<br>");
            details.append("User first name: ").append(user.getFirstName()).append("<br>");
            details.append("User last name: ").append(user.getLastName()).append("<br>");
            details.append("User phone: ").append(user.getPhone()).append("<br>");
            details.append("User address: ").append(user.getAddress()).append("<br>").append("<br>");
            details.append("<b>About company:</b> ").append("<br>").append("<br>");
            details.append("Company name: ").append(user.getCompanyName()).append("<br>");
            details.append("Company email: ").append(user.getCompanyEmail()).append("<br>");
            details.append("Company phone: ").append(user.getCompanyPhone()).append("<br>");
            details.append("Company address: ").append(user.getCompanyAddress()).append("<br>");
            details.append("Company description: ").append(user.getCompanyDescription()).append("<br>");

            List<Category> categories = user.getCategories();
            if (categories != null) {
                details.append("Categories: ");

                for (int i = 0; i < categories.size(); i++) {
                    Category category = categories.get(i);
                    details.append(category.Name);
                    if (i < categories.size() - 1) {

                        details.append(", ");
                    }
                }
                details.append("<br>");
            }

            List<EventType> et = user.getEventTypes();
            if(et!=null) {
                details.append("Event types: ");
                for (int i = 0; i < et.size(); i++) {
                    EventType e = et.get(i);
                    details.append(e.name);
                    if (i < et.size() - 1) {

                        details.append(", ");
                    }
                }
                details.append("<br>");
            }
            TextView textView = new TextView(context);
            textView.setText(Html.fromHtml(details.toString()));
            builder.setView(textView);
           /* builder.setMessage(details.toString());*/

            builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());

            builder.show();
        });
    }

    private void updateUserRegistrationStatus(UserRequest user) {
        executor.execute(() -> {
            user.setAdminVerified(true);
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("UsersRequest").child(user.id);
            userRef.child("adminVerified").setValue(true)
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.e("UserRequestAdapter", "Failed to update registration status: " + task.getException().getMessage());
                        }
                    });
        });
    }

}
