package com.example.eventplannerapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.EventType;
import com.example.eventplannerapp.model.Subcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventTypeEditActivity extends AppCompatActivity {
    EditText editTextDescription;
    String eventTypeId;
    private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private DatabaseReference subcategoriesRef;
    private Button addSubcategoryButton;
    private Spinner subcategorySpinner;
    private ListView subcategoryListView;
    private List<String> availableSubcategories;

    private List<Subcategory> allSubcategories;
    private List<String> recommendedSubcategories;
    private ArrayAdapter<String> subcategoryListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_event_type_edit);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        editTextDescription = findViewById(R.id.editTextDescription);
        Button updateButton = findViewById(R.id.buttonUpdate);

        Intent intent = getIntent();
        eventTypeId = intent.getStringExtra("EventTypeId");
        String eventTypeDescription = intent.getStringExtra("EventTypeDescription");

        editTextDescription.setText(eventTypeDescription);

        allSubcategories = new ArrayList<>();
        recommendedSubcategories = new ArrayList<>();
        subcategoriesRef = FirebaseDatabase.getInstance().getReference("Categories");

        addSubcategoryButton = findViewById(R.id.addSubcategoryButton);
        subcategorySpinner = findViewById(R.id.subcategorySpinner);
        subcategoryListView = findViewById(R.id.subcategoryListView);

        subcategoryListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recommendedSubcategories);
        subcategoryListView.setAdapter(subcategoryListAdapter);

        loadSubcategoriesNotAddedToEventType();

        addSubcategoryButton.setOnClickListener(v -> addSelectedSubcategoryToList());
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                executorService.submit(() -> {
                    updateEventTypeInFirebase();
                });
            }
        });
    }



    private void addSelectedSubcategoryToList() {
        String selectedSubcategoryName = subcategorySpinner.getSelectedItem().toString();

        if (!recommendedSubcategories.contains(selectedSubcategoryName)) {
            recommendedSubcategories.add(selectedSubcategoryName);
            subcategoryListAdapter.notifyDataSetChanged();


            availableSubcategories.remove(selectedSubcategoryName);
            ((ArrayAdapter)subcategorySpinner.getAdapter()).notifyDataSetChanged();
        } else {
            Toast.makeText(EventTypeEditActivity.this, "This subcategory is already added", Toast.LENGTH_LONG).show();
        }
    }

    private void loadSubcategoriesNotAddedToEventType() {
        DatabaseReference eventTypeRef = FirebaseDatabase.getInstance().getReference("EventTypes").child(eventTypeId);
        eventTypeRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    EventType eventType = dataSnapshot.getValue(EventType.class);
                    if (eventType != null) {
                        // Initialize the list if it's null
                        if (eventType.suggestedSC == null) {
                            eventType.suggestedSC = new ArrayList<>();
                        }

                        // Safely use the list
                        List<String> suggestedSubcategoryIds = new ArrayList<>();
                        for (Subcategory subcategory : eventType.suggestedSC) {
                            suggestedSubcategoryIds.add(subcategory.getId());
                        }

                        subcategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                availableSubcategories = new ArrayList<>();
                                allSubcategories = new ArrayList<>();

                                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                                    DataSnapshot subcategoriesSnapshot = categorySnapshot.child("Subcategories");

                                    for (DataSnapshot subcategorySnapshot : subcategoriesSnapshot.getChildren()) {
                                        Subcategory subcategory = subcategorySnapshot.getValue(Subcategory.class);
                                        if (subcategory != null && !suggestedSubcategoryIds.contains(subcategory.getId())) {
                                            allSubcategories.add(subcategory);
                                            availableSubcategories.add(subcategory.Name);
                                        }
                                    }
                                }

                                runOnUiThread(() -> {
                                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(EventTypeEditActivity.this,
                                            android.R.layout.simple_spinner_item, availableSubcategories);
                                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    subcategorySpinner.setAdapter(spinnerAdapter);
                                });
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                runOnUiThread(() -> Toast.makeText(EventTypeEditActivity.this, "Failed to load subcategories", Toast.LENGTH_LONG).show());
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                runOnUiThread(() -> Toast.makeText(EventTypeEditActivity.this, "Failed to load event type data", Toast.LENGTH_LONG).show());
            }
        });
    }




    private void updateEventTypeInFirebase() {
        String newDescription = editTextDescription.getText().toString();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("EventTypes").child(eventTypeId);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    EventType eventType = dataSnapshot.getValue(EventType.class);
                    if (eventType != null) {
                        if (eventType.suggestedSC == null) {
                            eventType.suggestedSC = new ArrayList<>();
                        }

                        List<Subcategory> suggestedSubcategories = eventType.suggestedSC;


                        if (allSubcategories == null) {
                            allSubcategories = new ArrayList<>();
                        }

                        for (String subcategoryName : recommendedSubcategories) {
                            for (Subcategory subcategory : allSubcategories) {
                               if (subcategory != null &&
                                        subcategory.Name != null &&
                                        subcategory.Name.equals(subcategoryName) &&
                                        !suggestedSubcategories.contains(subcategory)) {

                                    suggestedSubcategories.add(subcategory);
                                    break;
                                }
                            }
                        }

                        Map<String, Object> updates = new HashMap<>();
                        updates.put("description", newDescription);
                        updates.put("suggestedSC", suggestedSubcategories);

                        databaseReference.updateChildren(updates).addOnCompleteListener(task -> {
                            runOnUiThread(() -> {
                                if (task.isSuccessful()) {
                                    Toast.makeText(EventTypeEditActivity.this, "Event type updated successfully", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(EventTypeEditActivity.this, "Failed to update event type", Toast.LENGTH_SHORT).show();
                                }
                            });
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(EventTypeEditActivity.this, "Failed to fetch current data", Toast.LENGTH_SHORT).show();
            }
        });
    }


}