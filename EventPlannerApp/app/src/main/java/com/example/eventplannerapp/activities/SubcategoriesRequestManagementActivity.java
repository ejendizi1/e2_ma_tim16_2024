package com.example.eventplannerapp.activities;

import android.os.Bundle;

import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



import com.example.eventplannerapp.R;
import com.example.eventplannerapp.adapters.SubcategoryRequestAdapter;
import com.example.eventplannerapp.model.SubcategoryRequest;
import com.example.eventplannerapp.model.UserRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SubcategoriesRequestManagementActivity extends BaseActivity{
    private static final int PERMISSION_REQUEST_CODE = 100;
    private RecyclerView recyclerView;
    private SubcategoryRequestAdapter subAdapter;
    private ArrayList<SubcategoryRequest> subList;
    private DatabaseReference subsDbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContent(R.layout.activity_subcategory_request_management);


        recyclerView = findViewById(R.id.etRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        subList = new ArrayList<>();
        subAdapter = new SubcategoryRequestAdapter(subList,this);
        recyclerView.setAdapter(subAdapter);

        subsDbRef = FirebaseDatabase.getInstance().getReference().child("SubcategoriesRequest");



        subsDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                subList.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    SubcategoryRequest sub = dataSnapshot.getValue(SubcategoryRequest.class);

                    if (sub != null && sub.isAdminVerified()==false) {
                        subList.add(sub);
                    }
                }
                subAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(SubcategoriesRequestManagementActivity.this, "Failed to fetch data.", Toast.LENGTH_SHORT).show();
            }
        });
    }



}

