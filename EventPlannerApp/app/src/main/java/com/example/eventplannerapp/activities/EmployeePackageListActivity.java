package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;

public class EmployeePackageListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_employee_package_list);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void goToPackageListEmployee(View view) {
        Intent intent = new Intent(this, EmployeePackageListActivity.class);
        startActivity(intent);
    }

    public void goToServiceListEmployee(View view) {
        Intent intent = new Intent(this, EmployeeServiceListActivity.class);
        startActivity(intent);
    }
    public void goToProductListEmployee(View view) {
        Intent intent = new Intent(this, EmployeeProductListActivity.class);
        startActivity(intent);
    }

    public void goToPackageFilterEmployee(View view) {
        Intent intent = new Intent(this, EmployeePackageFilterActivity.class);
        startActivity(intent);
    }
}