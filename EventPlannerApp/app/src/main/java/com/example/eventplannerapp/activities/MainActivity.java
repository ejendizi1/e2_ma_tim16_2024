package com.example.eventplannerapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.eventplannerapp.R;
import com.example.eventplannerapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends BaseActivity {

    FirebaseAuth auth;
    Button logout;
    TextView userDetails;
    FirebaseUser user;
    private FirebaseAuth.AuthStateListener authListener;
    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    private DatabaseReference userRef;

    private SharedPreferences preferences;
    private static final String PREF_NAME = "UserPreferences";
    private static final String ROLE_KEY = "user_role";
    private static final String USER_ID_KEY = "user_id";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContent(R.layout.activity_main);
        EdgeToEdge.enable(this);
        //setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });


        auth= FirebaseAuth.getInstance();
        executorService.submit(this::cleanUpUnverifiedUsers);
        preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        logout=findViewById(R.id.buttonLogout);
        Button login=findViewById(R.id.buttonLogin);
        userDetails=findViewById(R.id.user_details);
        user=auth.getCurrentUser();
        authListener = firebaseAuth -> {
            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
            if (firebaseUser != null) {
                DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());
                login.setVisibility(View.GONE);
                usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            if (firebaseUser.isEmailVerified()) {
                                updateEmailVerificationStatus(firebaseUser);
                            }
                        } else {
                            FirebaseAuth.getInstance().signOut();
                            auth.signOut();
                            Log.e("LoginActivity", "User does not exist in the database.");
                            }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e("LoginActivity", "Database error: " + databaseError.getMessage());
                    }
                });
            }
        };

        auth.addAuthStateListener(authListener);
        if(user==null){
            userDetails.setText("Welcome");
            logout.setVisibility(View.GONE);
        }
        else{
            String welcomeMessage = "Welcome " + user.getEmail();
            userDetails.setText(welcomeMessage);
            fetchUserRoleAndSave(user.getUid());

        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                clearRole();
                clearUser();

                String role = preferences.getString(ROLE_KEY, "GUEST");
                Log.d("MainActivity", "Role after clear: " + role);

                userDetails.setText("Welcome");
                logout.setVisibility(View.GONE);
                login.setVisibility(View.VISIBLE);
                refreshNavigationDrawer();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
            executorService.shutdown();
        }
    }

    private void fetchUserRoleAndSave(String userId) {
        executorService.submit(() -> {
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);
            userRef.child("role").get().addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    String role = task.getResult().getValue(String.class);
                    Log.d("MainActivity", "Fetched role: " + role);

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(ROLE_KEY, role);
                    editor.putString(USER_ID_KEY, userId);
                    editor.apply();
                    Log.d("MainActivity", "Role saved successfully in SharedPreferences");

                    runOnUiThread(this::refreshNavigationDrawer);
                } else {
                    Log.e("MainActivity", "Failed to fetch role");
                }
            });
        });
    }



    private void cleanUpUnverifiedUsers() {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users");

        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);

                    if (user != null && !user.isEmailVerified()) {
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            Date sentTime = sdf.parse(user.getEmailSentTime());
                            long elapsedMillis = System.currentTimeMillis() - sentTime.getTime();


                            if (elapsedMillis > 24 * 60 * 60 * 1000) {
                                // Ukloni korisnika iz Realtime Database
                                userSnapshot.getRef().removeValue().addOnCompleteListener(removeTask -> {
                                    if (removeTask.isSuccessful()) {
                                        Log.d("MainActivity", "User removed from Realtime Database");

                                    } else {
                                        Log.e("MainActivity", "Failed to remove user from Realtime Database", removeTask.getException());
                                    }
                                });
                            }
                        } catch (ParseException e) {
                            Log.e("MainActivity", "Error parsing emailSentTime", e);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("MainActivity", "Failed to retrieve user data from Realtime Database", databaseError.toException());
            }
        });
    }





    private void clearUser() {

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_ID_KEY);
        editor.apply();
        Log.d("MainActivity", "user cleared");
    }

    private void clearRole() {

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(ROLE_KEY);
        editor.apply();
        Log.d("MainActivity", "Role cleared");
    }
    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    public void goToOwner(View view) {
        Intent intent = new Intent(this, OwnerProductListActivity.class);
        startActivity(intent);
    }

    public void goToEmployee(View view) {
        Intent intent = new Intent(this, EmployeeProductListActivity.class);
        startActivity(intent);
    }


    public void createEvent(View view) {
        Intent intent = new Intent(this, CreateEventActivity.class);
        startActivity(intent);
    }


    private void updateEmailVerificationStatus(FirebaseUser user) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
        userRef.child("emailVerified").setValue(true);
    }


}

